var assert = require('assert');
describe("HelloWorld Module", function () { // it calls suit, also describe part of mocha frame work.
    it('shuld return -1 when "Hello" is missing', function () {// one test case
        assert.equal(-1,'Hallo World'.indexOf('Hello'));
    });
    it('should return 0 when sentences start with Hello', function () { // one test case
        assert.equal(0,"Hello World, how are you?".indexOf("Hello"));
    });
});